import NavBar from "../../components/navbar";
import RegisterPromoForm from "../../components/RegisterPromoForm";

const RegisterPromo = () => {
  return (
    <>
      <NavBar />
      <RegisterPromoForm />
    </>
  );
};

export default RegisterPromo;
