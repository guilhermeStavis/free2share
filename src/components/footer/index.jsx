import "./style.css";

const Footer = () => {
  return (
    <footer className="footer">
      <p className="devs">
        <a
          href="https://www.linkedin.com/in/alexandre-alfa-b427a6158/"
          target="blank"
        >
          Alexandre Alfa
        </a>{" "}
        -{" "}
        <a href="https://www.linkedin.com/in/carlosbentz/" target="blank">
          Carlos Bentz
        </a>{" "}
        -{" "}
        <a href="https://www.linkedin.com/in/guilherme-stavis/" target="blank">
          Guilherme Stavis
        </a>{" "}
        -{" "}
        <a
          href="https://www.linkedin.com/in/pedro-henrique-germano-silva"
          target="blank"
        >
          Pedro H. Germano
        </a>
      </p>
      <p className="kenzie">
        <a href="https://kenzie.com.br/" target="blank">
          Kenzie Academy Brasil
        </a>{" "}
        - 2021
      </p>
    </footer>
  );
};

export default Footer;
