import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router-dom";
import { useState } from "react";
import axios from "axios";
import Cookies from "js-cookie";

import { setTokenThunk } from "../../store/modules/token/thunk";
import { setUserThunk } from "../../store/modules/user/thunk";
import { useDispatch } from "react-redux";

import "./style.css";
import Input from "../black-input";
import Button from "../button";

export default function LoginForm() {
  const history = useHistory();
  const dispatch = useDispatch();

  const [checkbox, setCheckbox] = useState(false);

  const scheme = yup.object().shape({
    email: yup
      .string()
      .required("Campo Necessário")
      .email("Formato de email Inválido"),
    password: yup
      .string()
      .required("Campo Necessário")
      .min(6, "Mínimo de 6 caractéres")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})/,
        "Senha deve conter ao menos um caracter especial"
      ),
  });

  const { register, handleSubmit, errors } = useForm({
    resolver: yupResolver(scheme),
  });

  const handleForm = (data) => {
    axios
      .post("https://free-2-share-api.herokuapp.com/login", data)
      .then((res) => {
        dispatch(setTokenThunk(res.data.accessToken));
        dispatch(setUserThunk(res.data.accessToken));
        checkbox && Cookies.set("token", res.data.accessToken, { expires: 1 });
        history.push("/home");
      });
  };

  return (
    <form noValidate onSubmit={handleSubmit(handleForm)} className="login">
      <div className="form-input">
        <h1 className="title">
          Log <a>in</a>
        </h1>
        <Input
          name="email"
          label="Email"
          error={errors.email}
          helperText={errors.email?.message}
          inputRef={register}
        />
        <Input
          name="password"
          label="Senha"
          error={errors.password}
          helperText={errors.password?.message}
          inputRef={register}
          type="password"
        />
      </div>
      <label className="container" onChange={() => setCheckbox(!checkbox)}>
        Mantenha-me conectado
        <input type="checkbox" checked={checkbox} />
        <span className="checkmark"></span>
      </label>
      <Button type="submit">Entrar</Button>
      <span className="register-now">
        Não possui uma conta?{" "}
        <a className="register-now" onClick={() => history.push("/register")}>
          Cadastre-se
        </a>
      </span>
    </form>
  );
}
