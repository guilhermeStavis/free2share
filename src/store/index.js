import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import userToken from "./modules/token/reducer";
import userReducer from "./modules/user/reducer";
import promosReducer from "./modules/promos/reducer";
import listOfFavoritesReducer from './modules/favoriteList/reducer';

const reducers = combineReducers({
  token: userToken,
  user: userReducer,
  listFavorites: listOfFavoritesReducer,
  promos: promosReducer,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
