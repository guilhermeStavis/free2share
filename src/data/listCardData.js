export const listCardData = [
  {
    titulo: "CounterStrike: Global Offensive",
    plataforma: "Steam",
    link:
      "https://store.steampowered.com/app/730/CounterStrike_Global_Offensive/",
    img:
      "https://steamcdn-a.akamaihd.net/steam/apps/730/header.jpg?t=1610576424",
    preco: "FREE",
    ExpirateDate: "3 - Jan - 2021",
    nickname: "XxCleitinhoGodxX",
    userId: 1,
    id: 2,
  },
  {
    titulo: "Grand Theft Auto V",
    plataforma: "Epic",
    link:
      "https://www.epicgames.com/store/pt-BR/product/grand-theft-auto-v/home",
    img:
      "https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fgrand-theft-auto-v%2Fhome%2FGTAV_EGS_Artwork_1920x1080_Hero-Carousel_V06-1920x1080-1503e4b1320d5652dd4f57466c8bcb79424b3fc0.jpg?h=1080&resize=1&w=1920",
    preco: "FREE",
    ExpirateDate: "3 - Jan - 2021",
    nickname: "XxCleitinhoGodxX",
    userId: 1,
    id: 4,
  },
  {
    img:
      "https://cdn02.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_download_software_1/H2x1_NSwitchDS_Fortnite_Chapter2Season5_image1600w.jpg",
    titulo: "Fortnite - Battle Royale",
    plataforma: "Epic Games",
    preco: "Free",
    ExpirateDate: "3 - Jan - 2021",
    link: "https://www.epicgames.com/fortnite/pt-BR/home",
    isFavorite: false,
    nickname: "XxCleitinhoGodxX",
    userId: 3,
    id: 1,
  },
];
