import {useState} from 'react'

import {useForm} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers/yup'
import * as yup from 'yup'

import axios from 'axios'
import jwt_decode from "jwt-decode";
import { useSelector} from 'react-redux'
import {useHistory} from 'react-router-dom'


import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import {TextField, Button} from '@material-ui/core'
import {Container} from './style'

const RegisterPromoForm = () => {

  const [error,setError] = useState(false)
  const [open, setOpen] =  useState(false);
  const history = useHistory();
  const [sucessAcess, setSucessAcess] = useState(false)
  const token = useSelector((state=>state.token))
  const decoded = jwt_decode(token)
  

  const schema = yup.object().shape({
    nome: yup
    .string()
    .required("Campo Obrigatório"),
    preco: yup
    .string()
    .matches(/R\$ ?\d{1,3}(\.\d{3})*,\d{2}/,"Valor invalido")
    .required("Campo Obrigatório"),
    desenvolvedora: yup
    .string()
    .required("Campo Obrigatório"),
    linkPromo : yup
    .string()
    .matches(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi,"Url Invalida")
    .required("Campo Obrigatório"),
    avatar: yup
    .string()
    .matches(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi,"Url Invalida")
    .required("Campo Obrigatório"),
    dataExpiracao: yup
    .date()
    .required("Campo Obrigatório"),
  })

  const {register, handleSubmit, errors} = useForm({
    resolver:yupResolver(schema),
  })

  const sendPromo = async(body) =>{
    const data = `${body.dataExpiracao}`
    try{
      body = {
        img : body.avatar,
        titulo : body.nome,
        plataforma: body.desenvolvedora,
        preco: body.preco=== "R$ 00,00"?"R$ FREE":body.preco,
        ExpirateDate: `${data.slice(8,10)} - ${data.slice(4,7)} - ${data.slice(11,15)}`,
        Link: body.linkPromo,
        isFavorite: false,
        userId : decoded.sub,
      }
      
      console.log(body)
      const response = await axios.post("https://free-2-share-api.herokuapp.com/promos",body,{headers: {Authorization:`Bearer ${token}`}})
      setOpen(true);
      setSucessAcess(true)
      setTimeout(()=>{
        history.push('/home')
      })
      // console.log(response)
    }
    catch(err){
      setError(true)
      console.log(err)
    }
  }
  const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
    
  };


  return (
    <>
    <Container>
      <div className="h3-content">
        <h3>Hey!</h3>
        <h3>Welcome</h3>
      </div>
      {error && <span>Não é possivel cadastrar essa promoção, por favor tente novamente!.</span> }
      <form onSubmit ={handleSubmit(sendPromo)} className="Form">
      <div>Nova Promoção!</div>
        <TextField variant="outlined" placeholder= "Nome" label="Nome" type ="text" name ="nome" inputRef ={register}  error = {!!errors.nome} helperText = {errors.nome?.message}/>
        <TextField variant="outlined" placeholder= "Preço" label="Preço" defaultValue="R$ 00,00" type ="text" name ="preco" inputRef ={register}  error = {!!errors.preco} helperText = {errors.preco?.message}/>
        <TextField variant="outlined" placeholder= "Desenvolvedora" label="Desenvolvedora" type ="text" name ="desenvolvedora" inputRef ={register}  error = {!!errors.desenvolvedora} helperText = {errors.desenvolvedora?.message}/>
        <TextField variant="outlined" placeholder= "Link da Promoção" label="Link da Promoção" type ="url" name ="linkPromo" inputRef ={register}  error = {!!errors.linkPromo} helperText = {errors.linkPromo?.message}/>
        <TextField variant="outlined" placeholder= "Link para Foto" label="Link para Foto" type ="url" name ="avatar" inputRef ={register}  error = {!!errors.avatar} helperText = {errors.avatar?.message}/>
        <TextField variant="outlined"  label="Data de Validade" defaultValue="2021-01-01" type ="date" name ="dataExpiracao" inputRef ={register}  error = {!!errors.dataExpiracao} helperText = {errors.dataExpiracao?.message}/>
        <Button type ="submit">Adicionar</Button>
      </form>
      {sucessAcess && (
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
            You Have Been Registered Successfully
            </Alert>
          </Snackbar>
      )}
    </Container>
    </>
  )
}

export default RegisterPromoForm
