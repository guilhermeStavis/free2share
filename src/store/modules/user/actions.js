export const setUser = (user) => ({
  type: "@user/SET_USER",
  user,
});
