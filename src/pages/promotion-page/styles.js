import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  promotionTitle: {
    color: "#FAF1FF",
    fontSize: "40px",
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: 700,
    lineHeight: "60px",
    margin: "auto",
    textAlign: "left",
    width: "90%",
  },
  root: {
    fontFamily: "Rubik",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "@media (max-width: 900px)": {
      width: "100%",
    },
  },
  promoInfo: {
    backgroundColor: "#39345F",
    boxSizing: "border-box",
    width: "50vw",
    padding: "40px",
    paddingTop: 0,
    boxSizing: "border-box",
    "@media (max-width: 900px)": {
      width: "100vw",
      textAlign: "center",
    },
  },

  commentsSection: {
    backgroundColor: "#39345F",
    boxSizing: "border-box",
    width: "50vw",
    padding: "40px",
    paddingTop: 0,
    boxSizing: "border-box",

    "@media (max-width: 900px)": {
      width: "100%",
    },
  },

  commentBox: {
    backgroundColor: "#615a7d",
    width: "100%",
    marginTop: "2vh",
    minHeight: "10vh",
    borderRadius: "4px",
    "@media (max-width: 900px)": {
      marginTop: "5vh",
    },
  },

  commentAuthor: {
    fontFamily: "Rubik",
    fontStyle: "Medium",
    fontSize: "18px",
    lineHeight: "150%",
    align: "Left",
    verticalAlign: "Top",
    paragraphSpacing: "20px",
    color: "#FAF1FF",
    marginLeft: "5px",
  },

  commentText: {
    fontFamily: "Rubik",
    fontStyle: "Regular",
    fontSize: "16px",
    lineHeight: "150%",
    align: "Left",
    verticalAlign: "Top",
    paragraphSpacing: "20px",
    color: "#FAF1FF",
    marginLeft: "20px",
    "@media (max-width: 900px)": {
      marginLeft: "5px",
    },
  },

  inputComment: {
    display: "flex",
  },

  gamePhoto: {
    width: "50vw",
  },
  return: {
    color: "#9034D8",
    fontFamily: "Rubik",
    fontStyle: "Medium",
    fontSize: "18px",
    lineHeight: "27px",
    Align: "Left",
    verticalAlign: "Top",
    paragraphSpacing: "20px",
    cursor: "pointer",
    marginBottom: "16px",
  },
  likeIcon: {
    color: "#FAF1FF",
    fontSize: "40px",
    margin: "0 12px",
  },
  likeCounter: {
    fontFamily: "Rubik",
    fontSize: "15px",
    textAlign: "center",
    color: "#FAF1FF",
  },

  likeBox: {
    cursor: "pointer",
    width: "10%",
  },

  titleBox: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },

  inputBox: {
    backgroundColor: "#615a7d",
    borderRadius: "4px",
    width: "100%",
  },
  platformName: {
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: "500",
    fontSize: "18px",
    lineHeight: "150%",
    color: "#D2CAD7",
    paddingBottom: "16px",
  },
  price: {
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: "40px",
    lineHeight: "150%",
    color: "#FAF1FF",
  },

  expires: {
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "150%",
    color: "#FAF1FF",
    marginTop: "10px",
    borderBottom: "2px solid #FAF1FF",
    padding: "8px 0",
  },

  creator: {
    fontFamily: "Rubik",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "150%",
    color: "#FAF1FF",
    marginTop: "10px",
    borderBottom: "2px solid #FAF1FF",
    padding: "8px 0",
  },
}));

export default useStyles;
