import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import PersonalData from "../personal-data";
import { Provider } from "react-redux";
import store from "../../store";

describe("When everything is ok", () => {
  test("Should change the button text from 'Alterar' to 'Salvar'", () => {
    render(
      <Provider store={store}>
        <PersonalData></PersonalData>
      </Provider>
    );
    userEvent.click(screen.getByText("Alterar"));
    const changeText = screen.getByRole("button").innerHTML;
    expect(changeText).toMatch("Salvar");
  });
});
