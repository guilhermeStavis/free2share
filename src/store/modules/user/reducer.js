const userReducer = (state = "", action) => {
  switch (action.type) {
    case "@user/SET_USER":
      return action.user;

    default:
      return state;
  }
};

export default userReducer;
