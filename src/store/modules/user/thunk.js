import jwt_decode from "jwt-decode";
import axios from "axios";

import { setUser } from "./actions";

export const setUserThunk = (token) => (dispatch, getState) => {
  const decoded = jwt_decode(token);
  axios
    .get("https://free-2-share-api.herokuapp.com/users/" + decoded.sub, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .then((res) => {
      dispatch(setUser(res.data));
    });
};

export const updateUserThunk = (token, data) => (dispatch, getState) => {
  const { user } = getState();

  axios
    .put("https://free-2-share-api.herokuapp.com/users/" + user.id, data, {
      headers: {
        Authorization: "Bearer " + token,
      },
    })
    .then((res) => dispatch(setUser(res.data)));
};
